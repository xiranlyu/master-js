let sign = 'X';
let status = 'on';
let count = 0;

function clickToPlay (box) {
  if (box.innerHTML === ' ' && status === 'on' && count < 9) {
    count++;
    box.innerHTML = sign;
    switchSign();
    document.getElementById('round').innerHTML = 'Next round: ' + sign;
    winner();
  }
  if (count >= 9 && status === 'on') {
    document.getElementById('status').innerHTML = 'The game ends as a tie.';
  }
}

function switchSign () {
  sign === 'X' ? sign = 'O' : sign = 'X';
}

function reset () {
  document.getElementById('round').innerHTML = 'Next round: X';
  document.getElementById('status').innerHTML = 'Playing ^_^';
  sign = 'X';
  status = 'on';
  count = 0;
  const boxes = document.querySelectorAll('.box');
  for (let i = 0; i < boxes.length; i++) {
    boxes[i].innerHTML = ' ';
  };
}

function winner () {
  const box1 = document.getElementById('box1').innerHTML;
  const box2 = document.getElementById('box2').innerHTML;
  const box3 = document.getElementById('box3').innerHTML;
  const box4 = document.getElementById('box4').innerHTML;
  const box5 = document.getElementById('box5').innerHTML;
  const box6 = document.getElementById('box6').innerHTML;
  const box7 = document.getElementById('box7').innerHTML;
  const box8 = document.getElementById('box8').innerHTML;
  const box9 = document.getElementById('box9').innerHTML;
  if ((box1 !== ' ' && box1 === box2 && box2 === box3) || (box4 !== ' ' && box4 === box5 && box5 === box6) ||
  (box7 !== ' ' && box7 === box8 && box8 === box9) || (box1 !== ' ' && box1 === box4 && box4 === box7) ||
  (box2 !== ' ' && box2 === box5 && box5 === box8) || (box3 !== ' ' && box3 === box6 && box6 === box9) ||
  (box1 !== ' ' && box1 === box5 && box5 === box9) || (box3 !== ' ' && box3 === box5 && box5 === box7)) {
    status = 'over';
    switchSign();
    document.getElementById('status').innerHTML = 'Congratulations, ' + sign + ' wins!';
  }
}
